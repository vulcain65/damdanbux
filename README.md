<!-- @format -->

# Introduction

# Reference links

# Flux externe

![Tux, the Linux mascot](./doc/schema_01/schema_01.svg)

## install dev env

### golang in debian

```shell
sudo apt-get update
sudo apt-get -y upgrade
# visite https://golang.org/dl/
# wget https://dl.google.com/go/go1.15.2.linux-amd64.tar.gz
wget https://golang.org/dl/go1.15.6.linux-amd64.tar.gz
tar -xvf go1.15.6.linux-amd64.tar.gz
sudo mv go /usr/local
# Setup Go Environment
export GOROOT=/usr/local/go
export GOPATH=$HOME/Projects/Proj1
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
# Verify Go Installation
go version ---> go version go1.15.2 linux/amd64
go env
```

### gin

https://github.com/gin-gonic/gin#installation

```shell
go get -u github.com/gin-gonic/gin
```

### Run app

```shell
go run server.go
```

## Build app

```shell
go build server.go
```

## config files

### config.json <!-- TODO voir refresh dns -->

| propriete               | type   | comm.                                      |
| ----------------------- | ------ | ------------------------------------------ |
| box.box                 | string | fichier liste grappe box                   |
| registrar.private       | string | fichier registrar dns admin box            |
| registrar.public        | string | fichiers registrars pour gestion dns appli |
| dns.dsnadmin            | string | fichiers dns admin box local               |
| dns.dns                 | string | fichier dns appli                          |
| dnslocal                | string | fichier dns local                          |
| synchrosite.synchrosite | string | fichier config synchronisation sites       |

### box.box

| propriete | type   | comm.   |
| --------- | ------ | ------- |
| [].name   | string | nom box |
| [].dns    | string | dns box |
| [].port   | int    | port    |

### registrar.private

| propriete | type   | comm.                                              |
| --------- | ------ | -------------------------------------------------- |
| domain    | string | nom domaine                                        |
| name      | string | nom registrar                                      |
| token     | obj    | information pour acces api                         |
| refresh   | int    | frequence controle et mise a jour ip dns en minute |

### registrar.public

| propriete  | type   | comm.                                              |
| ---------- | ------ | -------------------------------------------------- |
| [].domain  | string | nom domaine                                        |
| [].name    | string | nom registrar                                      |
| [].token   | obj    | information pour acces api                         |
| [].refresh | int    | frequence controle et mise a jour ip dns en minute |

### dns.admin

| propriete | type   | comm.        |
| --------- | ------ | ------------ |
| domain    | string | nom domaine  |
| subdomain | string | sous domaine |
| refresh   | int    | refresh      |

### dns.dns

| propriete | type   | comm.                      |
| --------- | ------ | -------------------------- |
| domain    | string | nom domaine                |
| subdomain | string | sous domaine               |
| box       | string | box de rattachement        |
| synchro   | obj    | parametres synchronisation |

### syncrosite.syncrosite

| propriete     | type    | comm.      |
| ------------- | ------- | ---------- |
| [].domaine    | string  |            |
| [].subdomaine | string  |            |
| [].active     | boolean |            |
| [].config     | obj     | paramètres |
