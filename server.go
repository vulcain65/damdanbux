package main

import (
	"emdayo/damdanbox/admindns"
	"context"
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
	"scheduler"
	"sync"
	"time"
)

var (
	CfConfig string // fichier configuration
	Cf = make(map[string]string)
)

func server(wg *sync.WaitGroup, ctx context.Context) {

	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	r.GET("/stop", func(c *gin.Context) {
		//TODO: attendre fin shedule pour stopper
		defer wg.Done()
	})
	r.Run(":8082")
}

func dyndns(ctx context.Context) {
	fmt.Println("----> A")
	fmt.Println(admindns.Start(ctx))
	fmt.Println("----> C")
	//fmt.Println(admindns.AfficheSexe())
	//fmt.Print("dyndns : %s \n", time.Now().String())
}

func main() {
	//var flagconfig string
	flag.StringVar(&CfConfig, "config", "./config.json", "json file config ")
	flag.Parse()

	fmt.Printf("server : %s \n", time.Now().String())
	Cf["config"] = CfConfig
	fmt.Println("flagconfig:", Cf["config"], )
	fmt.Println("flagconfig:", CfConfig, )

	ctx := context.Background()
	worker := scheduler.NewScheduler()
	worker.Add(ctx, dyndns, time.Minute*1)

	go dyndns(ctx)

	var wg sync.WaitGroup

	wg.Add(1)
	go server(&wg, ctx)
	wg.Wait()

}
